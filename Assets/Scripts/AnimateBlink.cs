﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateBlink : MonoBehaviour
{
    private float alpha = 1.0f;
    private float t = 0.0f;
    private bool active = false;
    private SpriteRenderer renderer;
    public Treasure treasure;
    // Start is called before the first frame update
    void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!active)
        {
            t += 10f * Time.deltaTime;

            alpha = ((Mathf.Cos(0.8f * t) + 1f) / 2f) * ((Mathf.Sin(1.5f * t) + 1f) / 2f) * ((Mathf.Sin(3.7f * t) + 1f) / 2f);

        }
        else
        {
            alpha = 1f;
        }

        Color col = renderer.color;
        col.a = alpha;
        renderer.color = col;
    }

    public void activate()
    {
        treasure.activate();
        active = true;
        FMODUnity.RuntimeManager.PlayOneShot("event:/Fire", GetComponent<Transform>().position);
    }


    public void deactivate()
    {
        treasure.deactivate();
        active = false;
    }
}
