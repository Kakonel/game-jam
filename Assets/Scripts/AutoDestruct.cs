﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestruct : MonoBehaviour
{
    [SerializeField] float time=2;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DoSomething", time);
    }

    public void DoSomething()
    {
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
