﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    static public BlockController controller;
    static private GameObject bodyPlayer;
    public Rigidbody2D rigidbody;
    public bool freeze;
    public bool isFalling;


    private void Awake()
    {
        if (!controller)
        {
            GameObject controllerObject = GameObject.Find("BlockController");
            controller = controllerObject.GetComponent<BlockController>();
            bodyPlayer = GameObject.FindWithTag("Player");
        }


    }

    private void Update()
    {
        if(isFalling)
        {
            rigidbody.bodyType = RigidbodyType2D.Dynamic;

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.Equals(bodyPlayer) && controller.canPush() && !freeze)
        {
            rigidbody.bodyType = RigidbodyType2D.Dynamic;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.Equals(bodyPlayer) && !controller.canPush() && !isFalling)
        {
            rigidbody.bodyType = RigidbodyType2D.Static;
        }
        else if (collision.gameObject.Equals(bodyPlayer) && controller.canPush() && !freeze)
        {
            rigidbody.bodyType = RigidbodyType2D.Dynamic;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.Equals(bodyPlayer) && !isFalling)
        {
            rigidbody.bodyType = RigidbodyType2D.Static;
        }

    }

    
}
