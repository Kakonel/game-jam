﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    public CharacterController2D controller;
    static private GameObject bodyPlayer;
    static private Animator bodyAnimator;

    public bool canBePushed;
    public bool isPushing;

    private void Start()
    {
        bodyPlayer = GameObject.FindWithTag("Player");
        bodyAnimator = bodyPlayer.GetComponent<Animator>();
        canBePushed = false;
        isPushing = false;


    }

    private void Update()
    {
        controller.isPushing = isPushing;
        bodyAnimator.SetBool("isPushing", isPushing);
    }

    public GameObject getBodyPlayer()
    {
        return bodyPlayer;
    }

    public bool canPush()
    {
        return canBePushed&controller.isPushing;
    }

    public void allowPush()
    {
        canBePushed = true;
    }
    public void disallowPush()
    {
        canBePushed = false;
    }



}
