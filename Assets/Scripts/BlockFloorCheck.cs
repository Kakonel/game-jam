﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockFloorCheck : MonoBehaviour
{
    public Block block;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        block.isFalling = false;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        block.isFalling = true;
    }
}
