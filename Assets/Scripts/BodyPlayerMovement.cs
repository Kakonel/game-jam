﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;
    public float runSpeed = 40f;
    public Animator animator;
    public WallCheck wallCheck;
    public int animatorIndex;

    private float horizontalMove = 0f;
    private bool jump = false;
    private bool groundCheck = true;


    private void Start()
    {
        animator.SetInteger("Index", animatorIndex);
    }

    void Update()
    {

        if (controller.m_Grounded || (!controller.m_Grounded && !wallCheck.hitWall))
        {
            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        }
        else
        {
            horizontalMove = 0f;
        }

        animator.SetBool("isMoving", Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0f);

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }

    }

    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    
}
