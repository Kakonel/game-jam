﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    private bool state = true;
    private GameObjectWizzard gameObjectWizzard = new GameObjectWizzard();
    [SerializeField] GameObject changedObject;
    [SerializeField] float time;
    [System.Serializable]
    public enum WhatToDo
    {
        destroy,
        disableObject,
        enableObject,
        switchStateObject,
        moveObject,
        kitemate
    }

    public WhatToDo whatToDo;
    // Start is called before the first frame update
    void Start()
    {

    }

    public string buttonClicked()
    {
        state = !state;
        gameObjectWizzard.whatToDo((int)whatToDo, changedObject, state);
        if(time!=0)
        {
            Invoke("buttonUnClick", time);
        }
        return name;
    }
    private void buttonUnClick()
    {
        state = !state;
        gameObjectWizzard.whatToDo((int)whatToDo, changedObject, state);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
