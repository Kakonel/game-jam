﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChaser : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] GameObject cameraObject;
    [SerializeField] GameObject player;
    float startPosX;
    [SerializeField] float endPosXDist;
    private float endPosX;


    void Start()
    {

    }

    private void Awake()
    {
        startPosX = cameraObject.transform.position.x;
        endPosX = startPosX + endPosXDist;
    }
    // Update is called once per frame
    void Update()
    {

        if (startPosX < player.transform.position.x+2 && endPosX > player.transform.position.x-2)
        {
            if (player.transform.position.x - 2 < Camera.main.transform.position.x)
            {

                Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(player.transform.position.x - 2, Camera.main.transform.position.y, Camera.main.transform.position.z), 3f * Time.deltaTime);
            }
            else
            {

            }
            if (player.transform.position.x + 2 > Camera.main.transform.position.x)
            {

                Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(player.transform.position.x + 2, Camera.main.transform.position.y, Camera.main.transform.position.z), 3f * Time.deltaTime);
            }
        }
    }
}
