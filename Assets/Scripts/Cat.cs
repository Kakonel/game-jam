﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour
{
    public bool look = false;
    public bool pounce = false;
    public Animator animator;

    // Update is called once per frame
    void Update()
    {
        if(look)
        {
            animator.SetBool("Look", true);
        }
        if(pounce)
        {
            animator.SetBool("Pounce", true);
        }
    }
}
