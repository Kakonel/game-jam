﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargingSpot : MonoBehaviour
{
    [SerializeField] List<GameObject> changedObjects;
    public bool isCharging=false;
    public float charge=0;
    public float chargeRate=1f;
    public float dischargeRate = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<SpriteRenderer>().color = charge*new Color(59f/255f, 78f / 255f, 255f / 255f, 255f / 255f);
        if (!isCharging)
        {
            charge -= dischargeRate * Time.deltaTime;
            charge = Mathf.Clamp(charge, 0, 1);
            if (charge == 0)
            {
                foreach(GameObject obj in changedObjects)
                {
                    obj.SetActive(true);
                }
            }
        }
        else
        {

            if (charge == 1)
            {
                foreach (GameObject obj in changedObjects)
                {
                    obj.SetActive(false);
                }
            }
        }

    }

    public float Charge()
    {

        isCharging = true;
        charge += chargeRate * Time.deltaTime;
        charge = Mathf.Clamp(charge, 0, 1);
        return charge;
    }
}
