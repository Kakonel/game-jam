﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chaser : MonoBehaviour
{
    [SerializeField] GameObject chasedObject;
    [SerializeField] float speed = 1;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 oldPos = transform.position;
        transform.position = Vector2.MoveTowards(transform.position, chasedObject.transform.position, speed*Time.deltaTime);
        Vector2 delta = new Vector2(transform.position.x-oldPos.x, transform.position.y - oldPos.y)*300;
        animator.SetFloat("speed", delta.magnitude/25f);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.AngleAxis(-delta.x, Vector3.forward), .05f);
        //transform.eulerAngles = Vector3.forward * -delta.x;
        // transform.rotation = Quaternion.Lerp(transform.rotation,new Quaternion((Vector3.forward * -delta.x).x, (Vector3.forward * -delta.y).y,0,1), Time.deltaTime * 100);
    }
}
