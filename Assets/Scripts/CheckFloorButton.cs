﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckFloorButton : MonoBehaviour
{
    public FloorButton button;
    public SpriteRenderer rend;

    private void Start()
    {
        rend = gameObject.GetComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        if(button.state)
        {
            rend.enabled = true;
        }
        else
        {
            rend.enabled = false;
        }
    }
}
