﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTrigger : MonoBehaviour
{
    public bool ready = false;
    public GameObject triggerObject;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.Equals(triggerObject))
        {
            ready = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.Equals(triggerObject))
        {
            ready = false;
        }
    }

}
