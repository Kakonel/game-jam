﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTriggers : MonoBehaviour
{
    public EndTrigger ghostTrigger;
    public EndTrigger playerTrigger;
    public SceneController controller;


    // Update is called once per frame
    void Update()
    {
        if(ghostTrigger.ready && playerTrigger.ready)
        {
            controller.loadNext = true;
        }
    }
}
