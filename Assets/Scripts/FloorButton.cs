﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorButton : MonoBehaviour
{
    public bool state = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("trigStart");
        if (collision.gameObject.tag == "Obstacle")
        {
      
            state = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Obstacle")
        {

            state = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
