﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectWizzard : MonoBehaviour
{
    public bool isMoving = false;
    public Vector2 finalPos;
    public float speed = 1;
    private GameObject movedObject;
    private float distX;
    private float distY;
    private bool wasMoved=false;


    public void whatToDo(int whatToDo, GameObject changedObject, bool state = false,float distX=2,float distY=2)
    {
        this.distX = distX;
        this.distY = distY;
        if (whatToDo == 0)//destroy
        {
            destroyObject(changedObject);
        }
        else
if (whatToDo == 1)//disableObject
        {
            disableObject(changedObject);
        }
        else
if (whatToDo == 2)//enableObject
        {
            enableObject(changedObject);
        }
        else
if (whatToDo == 3)//switchStateObject
        {
            switchStateObject(changedObject, state);
        }
        else
if (whatToDo == 4)//moveObject
        {
            moveObject(changedObject);
        }
        else//kitemate
            if (whatToDo == 5)
        {
            kitemate(changedObject, state);
        }
        else //unfreezeBlock
        if (whatToDo == 6)
        {
            unfreezeBlock(changedObject);
        }
        else//turnOnLight
        if (whatToDo==7)
        {
            turnOnLight(changedObject);
        }
        else //animateSpider
        if(whatToDo==8)
        {
            animateSpider(changedObject);
        }
    }

    public void destroyObject(GameObject destroyObject)
    {
        Destroy(destroyObject);
    }

    public void animateSpider(GameObject animated)
    {
        SpiderAnim anim = animated.GetComponent<SpiderAnim>();
        anim.go = true;
    }

    public void disableObject(GameObject disableObject)
    {
        disableObject.SetActive(false);
    }

    public void enableObject(GameObject disableObject)
    {
        disableObject.SetActive(true);
    }

    public void switchStateObject(GameObject disableObject, bool state)
    {
        disableObject.SetActive(state);
    }



    public void moveObject(GameObject movedObject)
    {
        if(!wasMoved)
        {
            wasMoved = true;

        this.movedObject = movedObject;

        finalPos = movedObject.transform.position+new Vector3(distX,distY,0);

        isMoving = true;
        }

    }

    public void moveUpdate(GameObject movedObject)
    {
        if(isMoving)
        {
            movedObject.transform.position = Vector2.MoveTowards(movedObject.transform.position, finalPos, 0.7f*Time.deltaTime);

            if(Vector2.Distance(movedObject.transform.position,finalPos)<0.01f)
            {
                isMoving = false;
            }
        }
    }

    public void kitemate(GameObject objectRigid, bool state)
    {
        objectRigid.GetComponent<Rigidbody2D>().isKinematic = state;
    }

    public void unfreezeBlock(GameObject blockObject)
    {
        Block block = blockObject.GetComponent<Block>();

        block.freeze = false;
        SpriteRenderer rend = blockObject.GetComponent<SpriteRenderer>();
        rend.color = Color.white;
    }

    public void turnOnLight(GameObject changedObject)
    {
        
        changedObject.transform.Find("Mask").gameObject.SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
