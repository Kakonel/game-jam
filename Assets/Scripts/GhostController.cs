﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        startPos = ghost.transform.position;//Input.mousePosition;
        mouseghost.transform.position = startPos;
        posDiff = startPos;
        Cursor.lockState = CursorLockMode.Confined;
    }
    private void Awake()
    {
        ghostScript = ghost.GetComponent<GhostMovement>();
        //particleSystem = ghost.transform.Find("ParticleEmiter").GetComponent<ParticleSystem>();
    }

    [SerializeField] public float distMax;
    [SerializeField] public float distMin;
    [SerializeField] public GameObject player;
    [SerializeField] public GameObject mouseghost;
    [SerializeField] public GameObject ghost;
    [SerializeField] public bool resetToPlayer;

    float leftSideOfScreen;
    float rightSideOfScreen;
    float upSideOfScreen;
    float downSideOfScreen;

    private GhostMovement ghostScript;
    private Vector3 startPos;
    private ParticleSystem particleSystem;
    public bool isControllable = true;
    private Vector2 pos;
    Vector3 lastMousePosition;
    private bool dontFlip=false;
    private bool m_FacingRight = true;
    public Vector2 resetPos = new Vector2(0, 0);
    Vector2 posDiff;
    private int frameBlocks = -1;


    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = ghost.transform.localScale;
        theScale.x *= -1;
        ghost.transform.localScale = theScale;
    }
    private void checkSide()
    {
        if (!dontFlip)
        {
            float dist = mouseghost.transform.position.x - ghost.transform.position.x;
                if (dist > 0 && !m_FacingRight)
                {
                    Flip();
                }
                else if (dist < 0 && m_FacingRight)
                {
                    Flip();
                }
            
        }

    }

    private void OnEnable()
    {
        ghostScript.switchChanged += switchChanged;
        ghostScript.buttonClicked += buttonClicked;
        ghostScript.chargingSpot += chargingSpot;
    }

    private void OnDisable()
    {
        ghostScript.switchChanged -= switchChanged;
        ghostScript.buttonClicked -= buttonClicked;
        ghostScript.chargingSpot -= chargingSpot;
    }

    void buttonClicked(string button)
    {
    }
    void switchChanged(bool state, string name,int id)
    {
    }
    void chargingSpot(float charge,string name)
    {

    }



    // Update is called once per frame
    private void resetMousePositionOnBorders()
    {
        Vector3 mWorld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2f, Screen.height/2f, 0f));

        if (Input.mousePosition.y >= Screen.height * 0.99 || Input.mousePosition.y <= 1)
        {
            Vector3 mouseWorld = mouseghost.transform.position;
            posDiff = new Vector2(mouseWorld.x, mouseWorld.y);
            Cursor.lockState = CursorLockMode.Locked;
            frameBlocks = 0;
            dontFlip = true;
            FMODUnity.RuntimeManager.PlayOneShot("event:/Spirit", GetComponent<Transform>().position);
        }
        else
        if (Input.mousePosition.x >= Screen.width * 0.99 || Input.mousePosition.x <= 1)
        {
            Vector3 mouseWorld = mouseghost.transform.position;
            posDiff = new Vector2(mouseWorld.x - mWorld.x, mouseWorld.y);
            Cursor.lockState = CursorLockMode.Locked;
            frameBlocks = 0;
            dontFlip = true;

        }
        else
        {
            checkSide();
            lastMousePosition = Input.mousePosition;
        }


    }

    void Update()
    {
        //Debug.Log(mouseghost.transform.position);
        float sensivity = 0.8f;
        if (isControllable)
        {

            Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseWorld.x *= sensivity;
            mouseWorld.y *= sensivity;
            mouseWorld.z *= sensivity;
           // particleSystem.
            //particleSystem.shape.arc=(Vector3.Angle(lastMousePosition,Input.mousePosition));

            pos = new Vector2(mouseWorld.x, mouseWorld.y) + posDiff + 0.2f*new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
            float dist = Vector2.Distance(ghost.transform.position, player.transform.position);

            leftSideOfScreen =  (Camera.main.transform.position.x -
                 Camera.main.orthographicSize * Screen.width / Screen.height);
            rightSideOfScreen = (Camera.main.transform.position.x +
                  Camera.main.orthographicSize * Screen.width / Screen.height);
            downSideOfScreen = (Camera.main.transform.position.y -
                  Camera.main.orthographicSize);
            upSideOfScreen = (Camera.main.transform.position.y +
                 Camera.main.orthographicSize);
            if (dist < distMax && dist > distMin)
            {
                if (pos.x < leftSideOfScreen)
                {
                    pos.x = leftSideOfScreen;
                }
                if (pos.x > rightSideOfScreen)
                {
                    pos.x = rightSideOfScreen;
                }
                if (pos.y < downSideOfScreen)
                {
                    pos.y = downSideOfScreen;
                }
                if (pos.y > upSideOfScreen)
                {
                    pos.y = upSideOfScreen;
                }
                mouseghost.transform.position = (pos);
            }
            else
            {
                ghostScript.caughtOnTrap();
                if (resetToPlayer)
                {
                    resetPos = player.transform.position;
                }
            }
            if (ghostScript.eventOccured)
            {
                if (resetToPlayer)
                {
                    resetPos = player.transform.position;
                }
                isControllable = false;
            }
            

        }
        else
        {
            if (Vector2.Distance(mouseghost.transform.position, resetPos) < 0.1f)
            {

                    Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    posDiff = resetPos - new Vector2(mouseWorld.x, mouseWorld.y);
                    ghostScript.eventOccured = false;
                    isControllable = true;
                    Cursor.lockState=CursorLockMode.None;
            }
            else
            {
                mouseghost.transform.position = Vector2.MoveTowards(mouseghost.transform.position, resetPos, 5f * Time.deltaTime);
            }


        }
        resetMousePositionOnBorders();
        if (frameBlocks > 1)
        {
            Cursor.lockState = CursorLockMode.None;
            frameBlocks = -1;
            dontFlip = false;
        }
        else
        {
            if (frameBlocks != -1)

            {
               
                frameBlocks++;
            }
            else
            {
               
            }
        }
       
    }
}
