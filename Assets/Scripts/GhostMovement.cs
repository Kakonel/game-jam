﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GhostMovement : MonoBehaviour
{
    public UnityAction<string> buttonClicked;
    public UnityAction<bool,string,int> switchChanged;
    public UnityAction<float, string> chargingSpot;

    public bool eventOccured = false;
    private bool mouseClicked = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void caughtOnTrap()
    {
        eventOccured = true;
        Cursor.lockState = CursorLockMode.Locked;
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.tag == "Trap")
        {
            caughtOnTrap();
        }
        //Cursor.visible = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "ChargingSpot")
        {
            chargingSpot.Invoke(collision.gameObject.GetComponent<ChargingSpot>().Charge(), collision.name);
        }
        if (Input.GetMouseButton(0))
        {

            if (!mouseClicked)
            {
                if (collision.tag == "Button")
                {
                    Debug.Log("Button");
                    buttonClicked.Invoke(collision.gameObject.GetComponent<Button>().buttonClicked());
                }
                else
                if (collision.tag == "Switch")
                {
                    Debug.Log("switch");
                    switchChanged.Invoke(collision.gameObject.GetComponent<Switch>().switchState(), collision.name, collision.gameObject.GetComponent<Switch>().id);
                }
            }
            mouseClicked = true;
        }
        else
        {
            mouseClicked = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "ChargingSpot")
        {
            collision.gameObject.GetComponent<ChargingSpot>().isCharging = false;
        }
    }



    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKey("up"))
        //{
        //    Cursor.lockState = CursorLockMode.Locked;


        //}

        //if (Input.GetKey("down"))
        //{

        //    Cursor.lockState = CursorLockMode.Confined;

        //}


    }
}
