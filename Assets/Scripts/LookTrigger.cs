﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookTrigger : MonoBehaviour
{
    public bool look = false;
    public Animator anim;
    public GameObject body;
    // Update is called once per frame
    void Update()
    {
        if(look)
        {
            anim.SetBool("Look", true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.Equals(body))
        {
            look = true;
        }
    }
}
