﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] GameObject endPos;
    private Vector2 startPos;
    private bool movingToEnd = true;
    [SerializeField] float speed = 1;

    void Start()
    {
        startPos = gameObject.transform.position; 
    }

    // Update is called once per frame
    void Update()
    {
        if(movingToEnd)
        {
            gameObject.transform.position = Vector2.MoveTowards(gameObject.transform.position, endPos.transform.position, speed * Time.deltaTime);
            if(Vector2.Distance(gameObject.transform.position,endPos.transform.position)<0.01f)
            {
                movingToEnd = false;
            }
        }
        else
        {
            gameObject.transform.position = Vector2.MoveTowards(gameObject.transform.position, startPos, speed * Time.deltaTime);
            if (Vector2.Distance(gameObject.transform.position, startPos) < 0.01f)
            {
                movingToEnd = true;
            }
        }
    }
}
