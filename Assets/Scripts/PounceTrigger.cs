﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PounceTrigger : MonoBehaviour
{
    public bool pounce = false;
    public Animator anim;
    public GameObject body;
    // Update is called once per frame
    void Update()
    {
        if (pounce)
        {
            anim.SetBool("Pounce", true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.Equals(body))
        {
            pounce = true;
        }
    }
}
