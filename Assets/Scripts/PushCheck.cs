﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushCheck : MonoBehaviour
{

    public Block block;
    static private GameObject bodyPlayer;

    private void Start()
    {

        bodyPlayer = GameObject.FindWithTag("Player");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.Equals(bodyPlayer))
        {
            Block.controller.isPushing = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.Equals(bodyPlayer))
        {
            Block.controller.isPushing = false;
        }
        if (collision.gameObject.Equals(bodyPlayer))
        {
            Block.controller.isPushing = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.Equals(bodyPlayer))
        {
            Block.controller.isPushing = false;
        }
    }
}
