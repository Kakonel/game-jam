﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour
{
    [SerializeField] float posXMin=-8;
    [SerializeField] float posYMin=-3;
    [SerializeField] float posXMax=8;
    [SerializeField] float posYMax=3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       transform.position = Vector2.MoveTowards(transform.position,new Vector2(Random.Range(posXMin, posXMax), Random.Range(posYMin, posYMax)), 1f * Time.deltaTime);
    }
}
