﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public string currentScene;
    public string nextScene;

    public bool loadNext;

    // Update is called once per frame
    void Update()
    {
       if(Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(currentScene);
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if(loadNext)
        {
            SceneManager.LoadScene(nextScene);

        }
    }
}
