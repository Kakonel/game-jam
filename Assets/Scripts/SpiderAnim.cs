﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderAnim : MonoBehaviour
{
    public Animator animator;
    public bool go = false;

    // Update is called once per frame
    void Update()
    {
        if(go)
        {
            animator.SetBool("Go", true);
        }
    }

    public void SpiderCrawlSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SpiderWalk", GetComponent<Transform>().position);
    }
}
