﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Switch : MonoBehaviour
{

    public bool state = true;
    private bool mouseClicked = false;
    public AnimateBlink blink;
    private GameObjectWizzard gameObjectWizzard = new GameObjectWizzard();
    [SerializeField] float distX;
    [SerializeField] float distY;
    [SerializeField] List<GameObject> changedObjects;
    [SerializeField] public int id;

    [SerializeField] bool activateOnce;

    private FMOD.Studio.EventInstance instance;

    [SerializeField]
    [Range(0f, 1f)]
    private float LightSwitch;

    [FMODUnity.EventRef]
    public string fmodEvent;

    public enum WhatToDo
    {
        destroy,
        disableObject,
        enableObject,
        switchStateObject,
        moveObject,
        kitemate,
        unfreezeBlock,
        turnOnLamp,
        animateSpider,
    }
    [SerializeField]
    public List<WhatToDo> whatToDo;
    //1
    // Start is called before the first frame update
    void Start()
    {
        //instance = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
        //instance.start();
    }
    public void deactivateBlink()
    {
        state = false;
        blink.deactivate();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {

                if (!mouseClicked)
                {
                    int a = 0;
                    foreach (GameObject item in changedObjects)
                    {
                        gameObjectWizzard.whatToDo((int)whatToDo[a], item, false, distX, distY);
                        a++;
                    }
                    if (blink)
                    {
                        blink.activate();
                    }
                }

                mouseClicked = true;

                FMODUnity.RuntimeManager.PlayOneShot("event:/Objects/Spotlight", GetComponent<Transform>().position);
                //instance.setParameterByName("LightSwitch", 1);
            }
            else
            {
                mouseClicked = false;
            }

        }


    }
    public bool switchState()
    {


        int a = 0;
        state = !state;
        foreach (GameObject item in changedObjects)
        {

            gameObjectWizzard.whatToDo((int)whatToDo[a], item, state, distX, distY);
            a++;
        }
        if (blink)
        {
            blink.activate();
        }



        return state;
    }

    // Update is called once per frame
    void Update()
    {
        int a = 0;
        foreach (GameObject item in changedObjects)
        {
            if (whatToDo[a] == WhatToDo.moveObject)

            {
                gameObjectWizzard.moveUpdate(item);
            }

            a++;
        }
    }


}

