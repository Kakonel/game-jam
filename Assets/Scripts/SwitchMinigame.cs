﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchMinigame : MonoBehaviour
{
    [SerializeField] List<Treasure> switches;
    [SerializeField] List<int> correctAnswer;
    private List<int> answers=new List<int>();
    private int correct=0;
    [SerializeField] GhostMovement ghostScript;

    private void resetStates()
    {
        foreach(Treasure item in switches)
        {
            item.transform.Find("Switch").GetComponent<Switch>().deactivateBlink();
        }
        correct = 0;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        ghostScript.switchChanged += switchChanged;

    }

    private void OnDisable()
    {
        ghostScript.switchChanged -= switchChanged;
    }


    void switchChanged(bool state, string name,int id)
    {
       

        if(correctAnswer[correct]==id)
        {
            correct++;
        }
        else
        {
            resetStates();
        }
        if(correct==correctAnswer.Count)
        {
            gameObject.SetActive(false);
        }
    }


    // Update is called once per frame
    void Update()
    {
       //for(int a=0;a<(correct+1);a++)
       // {
       //     Debug.Log(correctAnswer[a] + " "+switches[correctAnswer[a]].transform.Find("Switch").GetComponent<Switch>().state);
       //     if(switches[correctAnswer[a]].transform.Find("Switch").GetComponent<Switch>().state==true)
       //     {
       //         answers.Add(correctAnswer[a]);
       //         correct++;
       //     }
       //     else
       //     {
       //        //if( resetStates();
       //     }
       // }
       //if(correct==correctAnswer.Count)
       // {
       //     gameObject.SetActive(false);
       // }
    }
}
