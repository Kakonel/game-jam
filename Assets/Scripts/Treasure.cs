﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour
{

    public Color OffColor;
    public Color OnColor;
    private SpriteRenderer rend;

    // Start is called before the first frame update
    void Start()
    {
        rend = gameObject.GetComponent<SpriteRenderer>();
        rend.color = OffColor;
    }


    public void activate()
    {
        rend.color = OnColor;
    }
    public void deactivate()
    {
        rend.color = OffColor;
    }
}
