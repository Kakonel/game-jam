﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerLook : MonoBehaviour
{
    public Cat cat;
    public GameObject body;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.Equals(body))
        {
            cat.look = true;
        }
    }
}
