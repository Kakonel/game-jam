﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPounce : MonoBehaviour
{
    public Cat cat;
    public GameObject body;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.Equals(body))
        {
            cat.pounce = true;
        }
    }
}
