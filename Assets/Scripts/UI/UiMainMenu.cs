﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEngine.Events;

public class UiMainMenu : MonoBehaviour
{
    [SerializeField] GameObject musicButton;
    [SerializeField] GameObject musicPlay;
    public bool playMusic= true;
    [SerializeField] List<Sprite> imgs = new List<Sprite>();
    void Start()
    {
        
    }

    public void credits()
    {
        SceneManager.LoadScene("Credits");
    }
    public void start()
    {
        SceneManager.LoadScene("LevelOne");
    }
    public void mutePlay()
    {
        Debug.Log("reeee");
        playMusic = !playMusic;
        musicPlay.SetActive(playMusic);
        if(playMusic==true)
        {
            musicButton.GetComponent<Image>().sprite = imgs[0];
        }
        else
        {
            musicButton.GetComponent<Image>().sprite = imgs[1];
        }

    }

    public void exit()
    {
        Application.Quit();
    }

    //public void PlayHoverSound()
    //{
    //    FMODUnity.RuntimeManager.PlayOneShot("event:/Hover");
    //}
    //
    //public void PlayClickSound()
    //{
    //    FMODUnity.RuntimeManager.PlayOneShot("event:/Click");
    //}

    // Update is called once per frame
    void Update()
    {
        
    }
}
