﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCheck : MonoBehaviour
{
    public bool hitWall;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        hitWall = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        hitWall = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        hitWall = false;
    }

}
